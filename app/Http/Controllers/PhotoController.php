<?php

namespace App\Http\Controllers;

use App\Photo;
use http\Env\Response;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Photo::all()->take(10);
        return view("index")->with(['data'=>$data]);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        \Log::info($request->name);
        Photo::insert(['name'=>$request->name]);
        return "ok";
    }

    public function show($id)
    {
        $data=Photo::find($id);
        return response()->json($data);
        //return view("show")->with(['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Photo::where(['id'=>$id])->update(['name'=>$request->name]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo=Photo::where(['id'=>$id])->delete();
        return "ok";
    }
}
