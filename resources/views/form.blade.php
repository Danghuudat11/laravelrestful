<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Basic Table</h2>
    <p>The .table class adds basic styling (light padding and horizontal dividers) to a table:</p>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>NAME</th>
        </tr>
        </thead>
        <tbody id="tbodyTable">

        </tbody>
    </table>
    <button class="btn btn-primary" onclick="submitData()">Submit</button>
</div>
<script>
    function submitData(){
        var url = window.location.href;
        var id = url.split('/');
        var page = id[3];
        var urlAjax="";
        var data={};
        var method="";
        if(page=="add"){
            urlAjax="{{asset('photo')}}";
            data={
                name:$("#nameRecord").val()
            }
            method="POST";
        }
        else{
            id=id[4];
            urlAjax="{{asset('photo')}}/"+id;
            data={
                name:$("#nameRecord").val(),
            }
            method="PUT";
        }
        $.ajax({
            method: method,
            url: urlAjax,
            data:data,
            success: function(data) {
                alert("success")
            }
        })
    }
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = window.location.href;
        var id = url.split('/');
        var page = id[3];
        if (page == "add"){
            $("#tbodyTable").append('<td></td><td><input type="text" id = "nameRecord" class="form-control"></td>')
        }
        else{
            id=id[4];
            $.ajax({
                method: "get",
                url: "/photo/"+id,
                success: function(data) {
                    $("#tbodyTable").append('<tr><td id="idRecord">'+data.ID+'</td><td><input id = "nameRecord" type="text" value="'+data.name+'" class="form-control"></td></tr>')
                }
            })
        }

    })
</script>
</body>
</html>
