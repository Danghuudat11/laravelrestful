<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
<table class="table">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $dt)
        <tr>
            <td>{{$dt->ID}}</td>
            <td>{{$dt->name}}</td>
            <td><button onclick="updateRecord({{$dt->ID}})">Update</button></td>
            <td><button onclick="DeteleRecord({{$dt->ID}})">Delete</button></td>

        </tr>
    @endforeach
    </tbody>
</table>
</body>
<button class="btn btn-primary" onclick="addRecord()">ADD</button>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
    function updateRecord(id){
        window.location.href="{{asset('/edit')}}/"+id;
    }
    function addRecord(){
        window.location.href="{{asset('/add')}}";
    }
    function DeteleRecord(id) {
        if(confirm("Are you sure")){
            $.ajax({
                method: "DELETE",
                url: "/photo/"+id,
                success: function(data) {
                    alert("Delete complete");
                }
            })


        }
    }
</script>
</html>
